﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_Kolobov_LINQ
{
    class TaskStates
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
