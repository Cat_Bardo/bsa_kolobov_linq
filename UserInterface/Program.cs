﻿using System;
using SimpleCMenu.Menu;
using System.Drawing;
using Console = Colorful.Console;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BSA_Kolobov_LINQ;
using System.Linq;
using System.Xml;
using System.Linq.Expressions;

namespace UserInterface
{
    class Program
    {
        public static void Main(string[] args)
        {
            ProgramLogic.projects = ProgramLogic.GetProjectStruct().Result;
            Console.SetWindowSize(130, 30);
            // Title word
            string headerText = "       COOL LINQ      ";
            Console.Clear();
            // Setup the menu
            ConsoleMenu mainMenu = new ConsoleMenu();
            // Cool Parking title for all menues
            mainMenu.Header = headerText;
            // Main menu
            mainMenu.SubTitle = "-------------------------------------------------------------- Menu --------------------------------------------------------------";
            mainMenu.AddMenuItem(0, "1.Кiлькiсть таскiв у проектi конкретного користувача", Show1);
            mainMenu.AddMenuItem(1, "2.Cписок таскiв конкретного користувача(name таска < 45 символiв)", Show2);
            mainMenu.AddMenuItem(2, "3.Список таскiв, якi виконанi в поточному роцi для конкретного користувача", Show3);
            mainMenu.AddMenuItem(3, "4.Cписок команд, учасники яких старшi 10 рокiв, вiдсортованих за датою їх реєстрацiї за спаданням,згрупованих по командах", Show4);
            mainMenu.AddMenuItem(4, "5.Список користувачiв за алфавiтом first_name (по зростанню) з вiдсортованими tasks по довжинi name ", Show5);
            mainMenu.AddMenuItem(5, "6.Структура User", Show6);
            mainMenu.AddMenuItem(6, "7.Структура Project", Show7);
            mainMenu.AddMenuItem(7, "8.Вихiд", Exit);
            // Display the menu
            mainMenu.ShowMenu();
        }

        // Show methods for menu
        public static void Show1()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            Console.WriteLine("Введiть Id користувача:");
            int id = Convert.ToInt32(Console.ReadLine());
            try
            {
                foreach (var item in ProgramLogic.TaskCount(id))
                    Console.WriteLine($"Id проекта:{item.Key.Id}\tНазва проекта:{item.Key.Name,10} Кiлькiсть таскiв:{item.Value}");
            }
            catch (ArgumentException ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }

        public static void Show2()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            Console.WriteLine("Введiть Id користувача:");
            int id = Convert.ToInt32(Console.ReadLine());
            foreach (var item in ProgramLogic.Tasks(id))
                Console.WriteLine($"Id таска:{item.Id}\nНазва таска:{item.Name}\nСтан таска:{item.State}\n");
            Console.ReadKey();
        }

        public static void Show3()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            Console.WriteLine("Введiть Id користувача:");
            int id = Convert.ToInt32(Console.ReadLine());
            foreach (var item in ProgramLogic.FinishedTasks(id))
                Console.WriteLine($"Id таска:{item.Item1}\tНазва таска:{item.Item2}");
            Console.ReadKey();
        }

        public static void Show4()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            foreach (var item in ProgramLogic.TeamMembersOverTenYearsOld())
            {
                Console.WriteLine($"\n\nId команди:{item.Item1}\nНазва команди:{item.Item2}");
                Console.Write("Список користувачiв:\n");
                foreach (var user in item.Item3)
                    Console.Write($"{user.FirstName} Дата реєстрацiї:{user.RegisteredAt}\n");
            }
            Console.ReadKey();
        }

        public static void Show5()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            foreach (var item in ProgramLogic.AscendingUsersFirst_NameAndDescendingTaskName())
                Console.WriteLine($"Id користувача:{item.Item1.Id}\tIм'я користувача:{item.Item1.FirstName}\nНазва таску:{item.Item2.Name}\n");
            Console.ReadKey();
        }

        public static void Show6()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            Console.WriteLine("Введiть Id користувача:");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Iм'я користувача:{ProgramLogic.GetStructUser(id).Item1.FirstName}\nОстаннiй проект користувача:{ProgramLogic.GetStructUser(id).Item2.Name}\n" +
                                          $"Кiлькiсть таскiв пiд останнiм проектом:{ProgramLogic.GetStructUser(id).Item3}\nКiлькiсть незавершених або скасованих таскiв для користувача:{ProgramLogic.GetStructUser(id).Item4}\n" +
                                          $"Найтривалiший таск користувача за датою:{ProgramLogic.GetStructUser(id).Item5.Name}\n");

            Console.ReadKey();
        }

        public static void Show7()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            foreach (var item in ProgramLogic.GetStructProject())
            {
                Console.WriteLine($"Id проекту:{item.Item1.Id}\nНазва проекту:{item.Item1.Name}\n" +
                                  $"Назва найдовшого таску:{item.Item2.Name}\nНазва найкоротшого таску:{item.Item3.Name}\n" +
                                  $"Кiлькiсть користувачiв в командi проекту:{item.Item4}\n");
            }
            Console.ReadKey();
        }
        public static void Exit()
        {
            Environment.Exit(0);
        }
    }
}
